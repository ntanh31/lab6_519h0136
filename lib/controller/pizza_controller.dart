import 'package:lab6_foodlist/http/httprequest.dart';

import '../models/pizza.dart';
import '../http/httprequest.dart';

class PizzaController {
  Future<List<Pizza>> callPizzas() async {
    print('Use HttpHelper to get list of pizzas...');
    HttpRequest request = HttpRequest();
    List<Pizza> pizzas = await request.getPizzaList();

    return pizzas;
  }
}
